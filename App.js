import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import HomeScreen from "./src/home";
import ImageMoviment from "./src/imagemMoviment";


const Stack = createStackNavigator();

export default function App() {
  return(
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen"> 
      <Stack.Screen name="HomeScreen" component={HomeScreen}></Stack.Screen>
      <Stack.Screen name= "CountMoviment" component={CountMoviment}></Stack.Screen>
      <Stack.Screen name="ImageMoviment" component={ImageMoviment}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};